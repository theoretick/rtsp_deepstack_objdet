FROM golang:1.15-alpine AS build

ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /go/src
COPY . .
RUN go build -o /grabber

FROM alpine:3

RUN apk add --no-cache curl ffmpeg bash

COPY --from=build /grabber /grabber

ENTRYPOINT ["/grabber"]


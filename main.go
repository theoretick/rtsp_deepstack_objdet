package main

import (
  "bytes"
  "encoding/json"
  "fmt"
  "io/ioutil"
  "mime/multipart"
  "net/http"
  "os"
  "os/exec"
  "time"
)
const (
)

var (
  deepstackURL = os.Getenv("DEEPSTACK_URL")
  feedURL      = os.Getenv("RTSP_URL")
)

// generateSnapshot uses ffmpeg to generate a single frame from
// a given RTSP feed
func generateSnapshot(filename string) error {
  cmd := exec.Command(
    "ffmpeg", "-loglevel", "panic", "-y", "-i", feedURL, "-r", "1", "-vsync", "1", "-qscale", "1", "-f", "image2", "-frames:v", "1", filename,
  )
  return cmd.Run()
}

// processImage pings deepStack with imagesnapshot, returning
// results of object detection
func processImage(filename string) ([]byte, error) {
  file, err := os.Open(filename)
	if err != nil {
		return []byte{}, err
	}
	fileContents, err := ioutil.ReadAll(file)
	if err != nil {
		return []byte{}, err
	}
	fi, err := file.Stat()
	if err != nil {
		return []byte{}, err
	}
	file.Close()

	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("image", fi.Name())
	if err != nil {
		return []byte{}, err
	}
	part.Write(fileContents)

	err = writer.Close()
	if err != nil {
		return []byte{}, err
	}

  req, err := http.NewRequest("POST", deepstackURL, body)
  req.Header.Set("Content-Type", writer.FormDataContentType())

  client := &http.Client{}
  resp, err := client.Do(req)
  if err != nil {
      panic(err)
  }
  defer resp.Body.Close()

  if resp.StatusCode != http.StatusOK {
    fmt.Println("response Status:", resp.Status)
    return []byte{}, fmt.Errorf("deepstack response status %s: %v", resp.Status, resp.Body)
  }

  return ioutil.ReadAll(resp.Body)
}

// checkImage formats the datastream as pretty-JSON.
// this will later filter things accordingly
func checkImage(filename string, data []byte) (string, error) {
  var prettyJSON bytes.Buffer
  err := json.Indent(&prettyJSON, data, "", "  ")
  if err != nil {
      fmt.Println("JSON parse error: ", err)
      return "", err
  }

  return prettyJSON.String(), nil
}

func main() {
  for {
    filename := "snapshot-" + time.Now().Format("2006-01-02-150405") + ".jpg"

    fmt.Println("***********************************************************")
    fmt.Println(filename)

    err := generateSnapshot(filename)
    if err != nil{
      panic(err)
    }
    response, err := processImage(filename)
    if err != nil{
      continue
    }
    json, err := checkImage(filename, response)
    if err != nil{
      continue
    }
    fmt.Println(json)

    time.Sleep(1 * time.Second)
  }
}
